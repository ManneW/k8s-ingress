#! /bin/bash -ex

# Long timeout to wait for the Secret to appear as a certificate on
# the Pods.
kubectl wait --timeout=5m pod -l app=varnish-ingress --for=condition=Ready

# Delete the TLS Secret not used by the Ingress for Varnish
kubectl delete -f other-tls-secret.yaml

# Parse the controller log for this line:
# TLS Secret default/other-tls-secret not specified by any Ingress for Varnish, ignoring

# Get the name of the controller Pod
CTLPOD=$(kubectl get pods -n kube-system -l app=varnish-ingress-controller -o jsonpath={.items[0].metadata.name})

kubectl logs -n kube-system $CTLPOD | grep -q 'TLS Secret default/other-tls-secret not specified by any Ingress for Varnish, ignoring'
