/*
 * Copyright (c) 2019 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"context"
	"io/ioutil"
	"testing"

	api_v1 "k8s.io/api/core/v1"
	extensions "k8s.io/api/extensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes/fake"
	core_v1_listers "k8s.io/client-go/listers/core/v1"
	ext_listers "k8s.io/client-go/listers/extensions/v1beta1"
	"k8s.io/client-go/tools/cache"

	"github.com/sirupsen/logrus"
	logtest "github.com/sirupsen/logrus/hooks/test"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
)

func setupIngLister(
	client *fake.Clientset, ns string) ext_listers.IngressNamespaceLister {

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	infFactory := informers.NewSharedInformerFactory(client, 0)
	ingInformer := infFactory.Extensions().V1beta1().Ingresses().Informer()
	ingLister := infFactory.Extensions().V1beta1().Ingresses().Lister()
	ingNsLister := ingLister.Ingresses(ns)

	infFactory.Start(ctx.Done())
	cache.WaitForCacheSync(ctx.Done(), ingInformer.HasSynced)
	return ingNsLister
}

func TestIngsForTLSSecret(t *testing.T) {
	ns := "test-ns"
	ingClass := "viking"
	ingName := "viking-tls-ingress"
	secretName := "viking-tls-secret"
	wrongIngClassSecret := "wrong-ing-class-secret"
	noIngClassSecret := "no-ing-class-secret"

	client := fake.NewSimpleClientset(
		&extensions.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      ingName,
				Annotations: map[string]string{
					ingressClassKey: ingClass,
				},
			},
			Spec: extensions.IngressSpec{
				TLS: []extensions.IngressTLS{{
					SecretName: secretName,
				}},
			},
		},
		&extensions.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      "wrong-ing-class",
				Annotations: map[string]string{
					ingressClassKey: "wrong-ing-class",
				},
			},
			Spec: extensions.IngressSpec{
				TLS: []extensions.IngressTLS{{
					SecretName: wrongIngClassSecret,
				}},
			},
		},
		&extensions.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      "no-ing-class",
			},
			Spec: extensions.IngressSpec{
				TLS: []extensions.IngressTLS{{
					SecretName: noIngClassSecret,
				}},
			},
		},
		&extensions.Ingress{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      "different-secret-name",
				Annotations: map[string]string{
					ingressClassKey: ingClass,
				},
			},
			Spec: extensions.IngressSpec{
				TLS: []extensions.IngressTLS{{
					SecretName: "different-secret-name",
				}},
			},
		},
	)

	ingNsLister := setupIngLister(client, ns)
	worker := &NamespaceWorker{
		log:      &logrus.Logger{Out: ioutil.Discard},
		ing:      ingNsLister,
		ingClass: ingClass,
	}

	secret := &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      secretName,
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, err := worker.getIngsForTLSSecret(secret)
	if err != nil {
		t.Fatal("getIngsForTLSSecret(): ", err)
	}
	if len(ings) != 1 {
		t.Errorf("getIngsForTLSSecret(): wanted 1 Ingress, got %d",
			len(ings))
	}
	if ings[0].Name != ingName {
		t.Errorf("getIngsForTLSSecret(): Ingress name wanted %s, "+
			"got %s", ingName, ings[0].Name)
	}
	if is, err := worker.isVikingIngressTLSSecret(secret); err != nil {
		t.Fatal("isVikingIngressTLSSecret(): ", err)
	} else if !is {
		t.Error("isVikingIngressTLSSecret(): wanted true, got false")
	}

	secret = &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      "no-ing-for-secret",
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, err = worker.getIngsForTLSSecret(secret)
	if err != nil {
		t.Fatal("getIngsForTLSSecret(): ", err)
	}
	if ings != nil || len(ings) != 0 {
		t.Error("getIngsForTLSSecret(): wanted no Ingresses, got >0")
	}
	if is, err := worker.isVikingIngressTLSSecret(secret); err != nil {
		t.Fatal("isVikingIngressTLSSecret(): ", err)
	} else if is {
		t.Error("isVikingIngressTLSSecret(): wanted false, got true")
	}

	secret = &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      wrongIngClassSecret,
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, err = worker.getIngsForTLSSecret(secret)
	if err != nil {
		t.Fatal("getIngsForTLSSecret(): ", err)
	}
	if ings != nil || len(ings) != 0 {
		t.Error("getIngsForTLSSecret(): wanted no Ingresses, got >0")
	}
	if is, err := worker.isVikingIngressTLSSecret(secret); err != nil {
		t.Fatal("isVikingIngressTLSSecret(): ", err)
	} else if is {
		t.Error("isVikingIngressTLSSecret(): wanted false, got true")
	}

	secret = &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      noIngClassSecret,
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, err = worker.getIngsForTLSSecret(secret)
	if err != nil {
		t.Fatal("getIngsForTLSSecret(): ", err)
	}
	if ings != nil || len(ings) != 0 {
		t.Error("getIngsForTLSSecret(): wanted no Ingresses, got >0")
	}
	if is, err := worker.isVikingIngressTLSSecret(secret); err != nil {
		t.Fatal("isVikingIngressTLSSecret(): ", err)
	} else if is {
		t.Error("isVikingIngressTLSSecret(): wanted false, got true")
	}
}

func TestNoIngsForTLSSecret(t *testing.T) {
	ns := "test-ns"
	ingClass := "viking"

	client := fake.NewSimpleClientset()
	ingNsLister := setupIngLister(client, ns)
	worker := &NamespaceWorker{
		log:      &logrus.Logger{Out: ioutil.Discard},
		ing:      ingNsLister,
		ingClass: ingClass,
	}
	secret := &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      "test-secret",
		},
		Type: api_v1.SecretTypeTLS,
	}
	ings, err := worker.getIngsForTLSSecret(secret)
	if err != nil {
		t.Fatal("getIngsForTLSSecret(): ", err)
	}
	if len(ings) != 0 {
		t.Errorf("getIngsForTLSSecret(): wanted 0 Ingresses, got %d",
			len(ings))
	}
	if is, err := worker.isVikingIngressTLSSecret(secret); err != nil {
		t.Fatal("isVikingIngressTLSSecret(): ", err)
	} else if is {
		t.Error("isVikingIngressTLSSecret(): wanted false, got true")
	}
}

func setupSecrLister(
	ctx context.Context, client *fake.Clientset, ns string,
) core_v1_listers.SecretNamespaceLister {
	infFactory := informers.NewSharedInformerFactory(client, 0)
	secrInformer := infFactory.Core().V1().Secrets().Informer()
	secrLister := infFactory.Core().V1().Secrets().Lister()
	secrNsLister := secrLister.Secrets(ns)

	infFactory.Start(ctx.Done())
	cache.WaitForCacheSync(ctx.Done(), secrInformer.HasSynced)
	return secrNsLister
}

func TestDeletePEMSecret(t *testing.T) {
	ns := "test-ns"

	ingTLSSecret := &api_v1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: ns,
			Name:      "viking-ingress-tls-secret",
		},
	}
	spec := haproxy.SecretSpec{
		Namespace: ingTLSSecret.ObjectMeta.Namespace,
		Name:      ingTLSSecret.ObjectMeta.Name,
	}
	pemName := spec.CertName()
	client := fake.NewSimpleClientset(
		&api_v1.Secret{
			ObjectMeta: metav1.ObjectMeta{
				Namespace: ns,
				Name:      certSecretName,
			},
			Data: map[string][]byte{
				pemName: []byte("pem-data"),
			},
		},
	)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	secrNsLister := setupSecrLister(ctx, client, ns)

	worker := &NamespaceWorker{
		client: client,
		log:    &logrus.Logger{Out: ioutil.Discard},
		vsecr:  secrNsLister,
	}
	err := worker.deleteTLSSecret(ingTLSSecret)
	if err != nil {
		t.Fatal("deleteTLSSecret(): ", err)
	}
	updSecret, err := worker.vsecr.Get(certSecretName)
	if err != nil {
		t.Fatalf("Get(%s) after deleteTLSSecret(): %+v", certSecretName,
			err)
	}
	if val, ok := updSecret.Data[pemName]; ok {
		t.Errorf("Secret %s/%s field %s after deleteTLSSecret(), "+
			"expected no value, got: %s", ns, certSecretName,
			pemName, val)
	}
}

func TestDeleteNoPEMSecret(t *testing.T) {
	ns := "test-ns"

	ingTLSSecret := &api_v1.Secret{}
	client := fake.NewSimpleClientset()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	secrNsLister := setupSecrLister(ctx, client, ns)

	logger, hook := logtest.NewNullLogger()
	worker := &NamespaceWorker{
		client:    client,
		namespace: ns,
		log:       logger,
		vsecr:     secrNsLister,
	}
	worker.log.Level = logrus.TraceLevel
	err := worker.deleteTLSSecret(ingTLSSecret)
	if err != nil {
		t.Fatal("deleteTLSSecret(): ", err)
	}
	logEntry := hook.LastEntry()
	if logEntry == nil {
		t.Fatal("deleteTLSSecret(): no log entry")
	}
	if logEntry.Level != logrus.ErrorLevel {
		t.Errorf("deleteTLSSecret() log level wanted Error got %s",
			logEntry.Level)
	}
	msg := "PEM Secret " + ns + "/" + certSecretName +
		" not found, not requeuing"
	if logEntry.Message != msg {
		t.Errorf("deleteTLSSecret() log entry wanted [%s] got [%s]",
			msg, logEntry.Message)
	}
}
