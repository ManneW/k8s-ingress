/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"fmt"

	vcr_v1alpha1 "code.uplex.de/uplex-varnish/k8s-ingress/pkg/apis/varnishingress/v1alpha1"
	api_v1 "k8s.io/api/core/v1"
	extensions "k8s.io/api/extensions/v1beta1"

	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
)

// XXX make these configurable
const (
	admSecretKey         = "admin"
	dplaneSecretKey      = "dataplaneapi"
	certSecretName       = "tls-cert"
	vikingSecretLabelKey = vikingLabelPfx + "secret"
	vikingAdmSecretVal   = "admin"
	vikingPemSecretVal   = "pem"
	vikingAuthSecretVal  = "auth"
)

var (
	vikingAdmSecretSet = labels.Set(map[string]string{
		vikingSecretLabelKey: vikingAdmSecretVal,
	})
	// Selector for use in List() operations to find Secrets
	// designated as admin secrets for this application (Varnish
	// shared secret and dataplane API password for Basic Auth).
	vikingAdmSecretSelector = labels.SelectorFromSet(vikingAdmSecretSet)
)

func isPemSecret(secret *api_v1.Secret) (bool, error) {
	if secret.Name == certSecretName {
		if val, ok := secret.Labels[vikingSecretLabelKey]; !ok {
			return false, fmt.Errorf("Secret %s/%s lacks required "+
				"label %s", secret.Namespace, secret.Name,
				vikingSecretLabelKey)
		} else if val != vikingPemSecretVal {
			return false, fmt.Errorf("Secret %s/%s lacks required "+
				"label value %s=%s", secret.Namespace,
				secret.Name, vikingSecretLabelKey,
				vikingPemSecretVal)
		}
		return true, nil
	}
	if val, ok := secret.Labels[vikingSecretLabelKey]; ok &&
		val == vikingPemSecretVal {
		return false, fmt.Errorf("Secret %s/%s: Secrets with label "+
			"value %s=%s must have name %s", secret.Namespace,
			secret.Name, vikingSecretLabelKey, vikingPemSecretVal,
			certSecretName)
	}
	return false, nil
}

// XXX client...Update(secret) returns Secret
// XXX client...Create(secret) if it's new?
func (worker *NamespaceWorker) updateCertSecret(spec *haproxy.SecretSpec) error {
	nsLister := worker.listers.tsecr.Secrets(spec.Namespace)
	tlsSecret, err := nsLister.Get(spec.Name)
	if err != nil {
		return err
	}
	if tlsSecret.Type != api_v1.SecretTypeTLS {
		return fmt.Errorf("Ingress TLS Secret %s/%s: type is %s, "+
			"should be %s",
			tlsSecret.ObjectMeta.Namespace,
			tlsSecret.ObjectMeta.Name, tlsSecret.Type,
			string(api_v1.SecretTypeTLS))
	}
	crt, ok := tlsSecret.Data["tls.crt"]
	if !ok {
		return fmt.Errorf("Ingress TLS Secret %s/%s: key tls.crt not "+
			"found", tlsSecret.ObjectMeta.Namespace,
			tlsSecret.ObjectMeta.Name)
	}
	key, ok := tlsSecret.Data["tls.key"]
	if !ok {
		return fmt.Errorf("Ingress TLS Secret %s/%s: key tls.key not "+
			"found", tlsSecret.ObjectMeta.Namespace,
			tlsSecret.ObjectMeta.Name)
	}

	nsLister = worker.listers.vsecr.Secrets(spec.Namespace)
	certSecret, err := nsLister.Get(certSecretName)
	if err != nil {
		return err
	}
	pem := string(crt)
	if crt[len(crt)-1] != byte('\n') {
		pem += "\n"
	}
	pem += string(key)
	certName := spec.CertName()
	if certSecret.Data == nil {
		certSecret.Data = make(map[string][]byte)
	}
	certSecret.Data[certName] = []byte(pem)

	worker.log.Infof("Updating Secret %s/%s with %s to add contents from "+
		"Ingress TLS Secret %s/%s", certSecret.ObjectMeta.Namespace,
		certSecret.ObjectMeta.Name, certName,
		tlsSecret.ObjectMeta.Namespace, tlsSecret.ObjectMeta.Name)
	_, err = worker.client.CoreV1().
		Secrets(certSecret.ObjectMeta.Namespace).Update(certSecret)
	if err != nil {
		return err
	}
	spec.UID = string(tlsSecret.ObjectMeta.UID)
	spec.ResourceVersion = tlsSecret.ObjectMeta.ResourceVersion
	return nil
}

func (worker *NamespaceWorker) getIngsForTLSSecret(
	secret *api_v1.Secret) (ings []*extensions.Ingress, err error) {

	nsIngs, err := worker.ing.List(labels.Everything())
	if err != nil {
		if errors.IsNotFound(err) {
			worker.log.Infof("No Ingresses found in namespace %s",
				worker.namespace)
			return ings, nil
		}
		return nil, err
	}

	for _, ing := range nsIngs {
		if !worker.isVarnishIngress(ing) {
			continue
		}
		for _, tls := range ing.Spec.TLS {
			if tls.SecretName == secret.Name {
				ings = append(ings, ing)
			}
		}
	}
	return ings, nil
}

func (worker *NamespaceWorker) isVikingIngressTLSSecret(
	secret *api_v1.Secret) (bool, error) {

	if ings, err := worker.getIngsForTLSSecret(secret); err != nil {
		return false, err
	} else if ings == nil || len(ings) == 0 {
		return false, nil
	}
	return true, nil
}

func (worker *NamespaceWorker) deleteTLSSecret(secret *api_v1.Secret) error {
	certSecret, err := worker.vsecr.Get(certSecretName)
	if err != nil {
		if errors.IsNotFound(err) {
			// XXX classify as fatal when we refactor error handling
			worker.log.Errorf("PEM Secret %s/%s not found, not "+
				"requeuing", worker.namespace, certSecretName)
			return nil
		}
		return err
	}

	spec := haproxy.SecretSpec{
		Namespace: secret.ObjectMeta.Namespace,
		Name:      secret.ObjectMeta.Name,
	}
	certName := spec.CertName()
	delete(certSecret.Data, certName)

	worker.log.Infof("Updating Secret %s/%s to delete %s and remove "+
		"contents from Ingress TLS Secret %s/%s",
		certSecret.ObjectMeta.Namespace, certSecret.ObjectMeta.Name,
		certName, secret.ObjectMeta.Namespace, secret.ObjectMeta.Name)
	_, err = worker.client.CoreV1().
		Secrets(certSecret.ObjectMeta.Namespace).Update(certSecret)
	return err
}

func (worker *NamespaceWorker) getVarnishSvcsForSecret(
	secretName string) ([]*api_v1.Service, error) {

	var secrSvcs []*api_v1.Service
	svcs, err := worker.svc.List(varnishIngressSelector)
	if err != nil {
		return secrSvcs, err
	}
	for _, svc := range svcs {
		pods, err := worker.getPods(svc)
		if err != nil {
			return secrSvcs,
				fmt.Errorf("Error getting pod information for "+
					"service %s/%s: %v", svc.Namespace,
					svc.Name, err)
		}
		if len(pods.Items) == 0 {
			continue
		}

		// The secret is meant for the service if a
		// SecretVolumeSource is specified in the Pod spec
		// that names the secret.
		pod := pods.Items[0]
		for _, vol := range pod.Spec.Volumes {
			if vol.Secret == nil {
				continue
			}
			if vol.Secret.SecretName == secretName {
				secrSvcs = append(secrSvcs, svc)
			}
		}
	}
	return secrSvcs, nil
}

func (worker *NamespaceWorker) updateVcfgsForSecret(secrName string) error {
	var vcfgs []*vcr_v1alpha1.VarnishConfig
	vs, err := worker.vcfg.List(labels.Everything())
	if err != nil {
		return err
	}
	for _, v := range vs {
		for _, auth := range v.Spec.Auth {
			if auth.SecretName == secrName {
				vcfgs = append(vcfgs, v)
			}
		}
	}
	if len(vcfgs) == 0 {
		worker.log.Infof("No VarnishConfigs found for secret: "+
			"%s/%s", worker.namespace, secrName)
		return nil
	}
	for _, vcfg := range vcfgs {
		worker.log.Infof("Requeuing VarnishConfig %s/%s "+
			"after update for secret %s/%s",
			vcfg.Namespace, vcfg.Name, worker.namespace, secrName)
		worker.queue.Add(&SyncObj{Type: Update, Obj: vcfg})
	}
	return nil
}

func (worker *NamespaceWorker) updateVarnishSvcsForSecret(
	svcs []*api_v1.Service, secretKey string) error {

	for _, svc := range svcs {
		svcKey := svc.Namespace + "/" + svc.Name
		if err := worker.vController.
			UpdateSvcForSecret(svcKey, secretKey); err != nil {

			return err
		}
	}
	return nil
}

func (worker *NamespaceWorker) enqueueIngsForTLSSecret(
	secret *api_v1.Secret) error {

	ings, err := worker.getIngsForTLSSecret(secret)
	if err != nil {
		return err
	}
	if len(ings) == 0 {
		worker.log.Infof("No Varnish Ingresses defined for TLS Secret "+
			"%s/%s", secret.Namespace, secret.Name)
		// syncCounters.WithLabelValues(worker.namespace, "Service",
		// 	"Ignore").Inc()
		return nil
	}

	for _, ing := range ings {
		worker.log.Infof("Varnish Ingress %s/%s uses TLS Secret %s/%s,"+
			" re-queueing", ing.Namespace, ing.Name,
			secret.Namespace, secret.Name)
		worker.queue.Add(&SyncObj{Type: Update, Obj: ing})
	}
	return nil
}

func (worker *NamespaceWorker) getDplaneSecret() (string, []byte, error) {
	secrets, err := worker.vsecr.List(vikingAdmSecretSelector)
	if err != nil {
		return "", nil, err
	}
	for _, secret := range secrets {
		data, exists := secret.Data[dplaneSecretKey]
		if !exists {
			continue
		}
		key := secret.Namespace + "/" + secret.Name
		return key, data, nil
	}
	return "", nil, nil
}

func (worker *NamespaceWorker) setSecret(secret *api_v1.Secret) error {
	secretData, exists := secret.Data[admSecretKey]
	if !exists {
		return fmt.Errorf("Secret %s/%s does not have key %s",
			secret.Namespace, secret.Name, admSecretKey)
	}
	secretKey := secret.Namespace + "/" + secret.Name
	worker.log.Tracef("Setting secret %s", secretKey)
	worker.vController.SetAdmSecret(secretKey, secretData)

	secretData, exists = secret.Data[dplaneSecretKey]
	if !exists {
		return nil
	}
	worker.hController.SetDataplaneSecret(secretKey, secretData)
	return nil
}

func (worker *NamespaceWorker) syncSecret(key string) error {
	worker.log.Infof("Syncing Secret: %s/%s", worker.namespace, key)
	secret, err := worker.vsecr.Get(key)
	if err != nil {
		if secret, err = worker.tsecr.Get(key); err != nil {
			return err
		}
	}

	if secret.Type == api_v1.SecretTypeTLS {
		return worker.enqueueIngsForTLSSecret(secret)
	}

	if isPem, err := isPemSecret(secret); err != nil {
		return err
	} else if isPem {
		worker.log.Infof("PEM Secret %s/%s: no sync necessary",
			secret.Namespace, secret.Name)
		syncCounters.WithLabelValues(worker.namespace, "Secret",
			"Ignore").Inc()
		return nil
	}

	secretType, ok := secret.Labels[vikingSecretLabelKey]
	if !ok {
		// Should be impossible due to informer filtering
		return fmt.Errorf("Secret %s/%s lacks required label %s",
			secret.Namespace, secret.Name, vikingSecretLabelKey)
	}
	if secretType == vikingAuthSecretVal {
		return worker.updateVcfgsForSecret(secret.Name)
	}
	if secretType != vikingAdmSecretVal {
		return fmt.Errorf("Secret %s/%s: unknown value %s for label %s",
			secret.Namespace, secret.Name, secretType,
			vikingSecretLabelKey)
	}

	svcs, err := worker.getVarnishSvcsForSecret(secret.Name)
	if err != nil {
		return err
	}
	worker.log.Tracef("Found Varnish services for secret %s/%s: %v",
		secret.Namespace, secret.Name, svcs)
	if len(svcs) == 0 {
		worker.log.Infof("No Varnish services with admin secret: %s/%s",
			secret.Namespace, secret.Name)
		return nil
	}

	err = worker.setSecret(secret)
	if err != nil {
		return err
	}
	secretKey := secret.Namespace + "/" + secret.Name
	return worker.updateVarnishSvcsForSecret(svcs, secretKey)
}

func (worker *NamespaceWorker) addSecret(key string) error {
	return worker.syncSecret(key)
}

func (worker *NamespaceWorker) updateSecret(key string) error {
	return worker.syncSecret(key)
}

func (worker *NamespaceWorker) deleteSecret(obj interface{}) error {
	secr, ok := obj.(*api_v1.Secret)
	if !ok || secr == nil {
		worker.log.Warnf("Delete Secret: not found: %v", obj)
		return nil
	}

	if secr.Type == api_v1.SecretTypeTLS {
		if is, err := worker.isVikingIngressTLSSecret(secr); err != nil {
			return err
		} else if !is {
			worker.log.Infof("TLS Secret %s/%s not specified by "+
				"any Ingress for Varnish, ignoring",
				secr.Namespace, secr.Name)
			return nil
		}
		worker.log.Infof("Deleting TLS Secret: %s/%s", secr.Namespace,
			secr.Name)
		return worker.deleteTLSSecret(secr)
	}

	// XXX quite a bit of code duplication with syncSecret above
	if isPem, err := isPemSecret(secr); err != nil {
		return err
	} else if isPem {
		worker.log.Infof("PEM Secret %s/%s: no sync necessary",
			secr.Namespace, secr.Name)
		syncCounters.WithLabelValues(worker.namespace, "Secret",
			"Ignore").Inc()
		return nil
	}

	secretType, ok := secr.Labels[vikingSecretLabelKey]
	if !ok {
		// Should be impossible due to informer filtering
		return fmt.Errorf("Secret %s/%s lacks required label %s",
			secr.Namespace, secr.Name, vikingSecretLabelKey)
	}
	if secretType == vikingAuthSecretVal {
		return worker.updateVcfgsForSecret(secr.Name)
	}
	if secretType != vikingAdmSecretVal {
		return fmt.Errorf("Secret %s/%s: unknown value %s for label %s",
			secr.Namespace, secr.Name, secretType,
			vikingSecretLabelKey)
	}

	worker.log.Infof("Deleting Secret: %s/%s", secr.Namespace, secr.Name)
	svcs, err := worker.getVarnishSvcsForSecret(secr.Name)
	if err != nil {
		return err
	}
	worker.log.Tracef("Found Varnish services for secret %s/%s: %v",
		secr.Namespace, secr.Name, svcs)
	if len(svcs) == 0 {
		worker.log.Infof("No Varnish services with admin secret: %s/%s",
			secr.Namespace, secr.Name)
		return nil
	}

	secretKey := secr.Namespace + "/" + secr.Name
	worker.vController.DeleteAdmSecret(secretKey)
	worker.hController.DeleteDataplaneSecret(secretKey)

	return worker.updateVarnishSvcsForSecret(svcs, secretKey)
}
