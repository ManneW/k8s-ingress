/*
 * Copyright (c) 2018 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package controller

import (
	"fmt"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/haproxy"
	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/varnish/vcl"

	api_v1 "k8s.io/api/core/v1"
	extensions "k8s.io/api/extensions/v1beta1"
	"k8s.io/apimachinery/pkg/labels"
)

// XXX make this configurable
const (
	admPortName     = "varnishadm"
	tlsPortName     = "tls"
	dplanePortName  = "dataplane"
	faccessPortName = "faccess"
)

// isVarnishIngSvc determines if a Service represents a Varnish that
// can implement Ingress, for which this controller is responsible.
// Currently the app label must point to a hardwired name, and a
// hardwired admin port name must be defined for one of the Endpoints.
func (worker *NamespaceWorker) isVarnishIngSvc(svc *api_v1.Service) bool {
	app, exists := svc.Labels[labelKey]
	if !exists || app != labelVal {
		return false
	}
	for _, port := range svc.Spec.Ports {
		worker.log.Debugf("Service %s/%s port: %+v", svc.Namespace,
			svc.Name, port)
		if port.Name == admPortName {
			return true
		}
	}
	return false
}

func (worker *NamespaceWorker) getIngsForSvc(
	svc *api_v1.Service) (ings []*extensions.Ingress, err error) {

	allIngs, err := worker.ing.List(labels.Everything())
	if err != nil {
		return
	}

	for _, ing := range allIngs {
		if ing.Namespace != svc.Namespace {
			// Shouldn't be possible
			continue
		}
		cpy := ing.DeepCopy()
		if cpy.Spec.Backend != nil {
			if cpy.Spec.Backend.ServiceName == svc.Name {
				ings = append(ings, cpy)
			}
		}
		for _, rules := range cpy.Spec.Rules {
			if rules.IngressRuleValue.HTTP == nil {
				continue
			}
			for _, p := range rules.IngressRuleValue.HTTP.Paths {
				if p.Backend.ServiceName == svc.Name {
					ings = append(ings, cpy)
				}
			}
		}
	}

	if len(ings) == 0 {
		worker.log.Infof("No Varnish Ingresses defined for service %s/%s",
			svc.Namespace, svc.Name)
		syncCounters.WithLabelValues(worker.namespace, "Service",
			"Ignore").Inc()
	}
	return ings, nil
}

func (worker *NamespaceWorker) enqueueIngressForService(
	svc *api_v1.Service) error {

	ings, err := worker.getIngsForSvc(svc)
	if err != nil {
		return err
	}
	for _, ing := range ings {
		if !worker.isVarnishIngress(ing) {
			continue
		}
		worker.queue.Add(&SyncObj{Type: Update, Obj: ing})
	}
	return nil
}

// Return true if changes in Varnish services may lead to changes in
// the VCL config generated for the Ingress.
func (worker *NamespaceWorker) isVarnishInVCLSpec(ing *extensions.Ingress) bool {
	vcfgs, err := worker.vcfg.List(labels.Everything())
	if err != nil {
		worker.log.Warnf("Error retrieving VarnishConfigs in "+
			"namespace %s: %v", worker.namespace, err)
		return false
	}
	for _, vcfg := range vcfgs {
		if vcfg.Spec.SelfSharding != nil {
			return true
		}
	}
	return false
}

func epAddrs2VCLAddrs(
	epAddrs []api_v1.EndpointAddress,
	vclAddrs []vcl.Address,
	admPort int32,
) []vcl.Address {
	for _, epAddr := range epAddrs {
		vclAddr := vcl.Address{
			IP:   epAddr.IP,
			Port: admPort,
		}
		vclAddrs = append(vclAddrs, vclAddr)
	}
	return vclAddrs
}

func epAddrs2OffldAddrs(
	epAddrs []api_v1.EndpointAddress,
	offldAddrs []haproxy.OffldAddr,
	dplanePort, faccessPort int32,
) []haproxy.OffldAddr {
	for _, epAddr := range epAddrs {
		offldAddr := haproxy.OffldAddr{
			IP:            epAddr.IP,
			DataplanePort: dplanePort,
			FaccessPort:   faccessPort,
		}
		if epAddr.TargetRef != nil {
			offldAddr.PodNamespace = epAddr.TargetRef.Namespace
			offldAddr.PodName = epAddr.TargetRef.Name
		}
		offldAddrs = append(offldAddrs, offldAddr)
	}
	return offldAddrs
}

func (worker *NamespaceWorker) syncSvc(key string) error {
	var addrs []vcl.Address
	var offldAddrs []haproxy.OffldAddr

	worker.log.Infof("Syncing Service: %s/%s", worker.namespace, key)
	svc, err := worker.svc.Get(key)
	if err != nil {
		return err
	}

	if !worker.isVarnishIngSvc(svc) {
		return worker.enqueueIngressForService(svc)
	}

	worker.log.Infof("Syncing Varnish Ingress Service %s/%s:",
		svc.Namespace, svc.Name)

	// Check if there are Ingresses for which the VCL spec may
	// change due to changes in Varnish services.
	updateVCL := false
	ings, err := worker.ing.List(labels.Everything())
	if err != nil {
		return err
	}
	for _, ing := range ings {
		if ing.Namespace != svc.Namespace {
			continue
		}
		ingSvc, err := worker.getVarnishSvcForIng(ing)
		if err != nil {
			return err
		}
		if ingSvc == nil {
			continue
		}
		worker.log.Debugf("Ingress %s/%s: Service %s/%s", ing.Namespace,
			ing.Name, svc.Namespace, svc.Name)
		if ingSvc.Namespace != svc.Namespace ||
			ingSvc.Name != svc.Name {
			continue
		}
		worker.log.Debugf("Ingress %s/%s: %+v", ing.Namespace, ing.Name,
			ing)
		svcKey := svc.Namespace + "/" + svc.Name
		if worker.vController.HasVarnishSvc(svcKey) &&
			!worker.isVarnishInVCLSpec(ing) {
			continue
		}
		updateVCL = true
		worker.log.Debug("Requeueing Ingress %s/%s after changed "+
			"Varnish service %s/%s: %+v", ing.Namespace,
			ing.Name, svc.Namespace, svc.Name, ing)
		worker.queue.Add(&SyncObj{Type: Update, Obj: ing})
	}
	if !updateVCL {
		worker.log.Debugf("No change in VCL due to changed Varnish "+
			"service %s/%s", svc.Namespace, svc.Name)
	}

	endps, err := worker.getServiceEndpoints(svc)
	if err != nil {
		return err
	}
	worker.log.Tracef("Varnish service %s/%s endpoints: %+v", svc.Namespace,
		svc.Name, endps)
	if endps == nil {
		return fmt.Errorf("could not find endpoints for service: %s/%s",
			svc.Namespace, svc.Name)
	}

	// Get the secret name and admin port for the service. We have
	// to retrieve a Pod spec for the service, then look for the
	// SecretVolumeSource, and the port matching admPortName.
	secrName := ""
	worker.log.Tracef("Searching Pods for the secret for %s/%s",
		svc.Namespace, svc.Name)
	pods, err := worker.getPods(svc)
	if err != nil {
		return fmt.Errorf("Cannot get a Pod for service %s/%s: %v",
			svc.Namespace, svc.Name, err)
	}
	if len(pods.Items) == 0 {
		return fmt.Errorf("No Pods for Service: %s/%s", svc.Namespace,
			svc.Name)
	}
	pod := &pods.Items[0]
	for _, vol := range pod.Spec.Volumes {
		if secretVol := vol.Secret; secretVol != nil {
			secrName = secretVol.SecretName
			break
		}
	}
	if secrName != "" {
		worker.log.Infof("Found secret name %s/%s for Service %s/%s",
			worker.namespace, secrName, svc.Namespace, svc.Name)

		if secret, err := worker.vsecr.Get(secrName); err == nil {
			err = worker.setSecret(secret)
			if err != nil {
				return err
			}
		} else {
			worker.log.Warnf("Cannot get Secret %s: %v", secrName,
				err)
		}
	} else {
		worker.log.Warnf("No secret found for Service %s/%s",
			svc.Namespace, svc.Name)
	}

	// XXX hard-wired Port names
	for _, subset := range endps.Subsets {
		admPort, dplanePort, faccessPort := int32(0), int32(0), int32(0)
		hasTLS := false
		for _, port := range subset.Ports {
			switch port.Name {
			case admPortName:
				admPort = port.Port
			case dplanePortName:
				hasTLS = true
				dplanePort = port.Port
			case faccessPortName:
				hasTLS = true
				faccessPort = port.Port
			}
		}
		if admPort == 0 {
			return fmt.Errorf("No Varnish admin port %s found for "+
				"Service %s/%s endpoint", admPortName,
				svc.Namespace, svc.Name)
		}
		addrs = epAddrs2VCLAddrs(subset.Addresses, addrs, admPort)
		addrs = epAddrs2VCLAddrs(subset.NotReadyAddresses, addrs,
			admPort)
		if hasTLS {
			offldAddrs = epAddrs2OffldAddrs(subset.Addresses,
				offldAddrs, dplanePort, faccessPort)
			offldAddrs = epAddrs2OffldAddrs(
				subset.NotReadyAddresses, offldAddrs,
				dplanePort, faccessPort)
		}
		// for _, address := range subset.Addresses {
		// 	addr := vcl.Address{
		// 		IP:   address.IP,
		// 		Port: admPort,
		// 	}
		// 	addrs = append(addrs, addr)
		// 	if hasTLS {
		// 		addr := haproxy.OffldAddr{
		// 			IP:            address.IP,
		// 			DataplanePort: dplanePort,
		// 			FaccessPort:   faccessPort,
		// 		}
		// 		if address.TargetRef != nil {
		// 			addr.PodNamespace =
		// 				address.TargetRef.Namespace
		// 			addr.PodName = address.TargetRef.Name
		// 		}
		// 		offldAddrs = append(offldAddrs, addr)
		// 	}
		// }
	}
	if len(offldAddrs) > 0 {
		worker.log.Tracef("Varnish service %s/%s offloader addresses: "+
			"%+v", svc.Namespace, svc.Name, offldAddrs)
		err = worker.hController.AddOrUpdateOffloader(
			svc.Namespace+"/"+svc.Name, offldAddrs,
			worker.namespace+"/"+secrName)
		if err != nil {
			return err
		}
	}
	worker.log.Tracef("Varnish service %s/%s addresses: %+v", svc.Namespace,
		svc.Name, addrs)
	return worker.vController.AddOrUpdateVarnishSvc(
		svc.Namespace+"/"+svc.Name, addrs,
		worker.namespace+"/"+secrName, !updateVCL)
}

func (worker *NamespaceWorker) addSvc(key string) error {
	return worker.syncSvc(key)
}

func (worker *NamespaceWorker) updateSvc(key string) error {
	return worker.syncSvc(key)
}

func (worker *NamespaceWorker) deleteSvc(obj interface{}) error {
	svc, ok := obj.(*api_v1.Service)
	if !ok || svc == nil {
		worker.log.Warnf("Delete Service: not found: %v", obj)
		return nil
	}
	nsKey := svc.Namespace + "/" + svc.Name
	worker.log.Info("Deleting Service: ", nsKey)

	if worker.vController.HasVarnishSvc(nsKey) {
		if err := worker.vController.DeleteVarnishSvc(nsKey); err != nil {
			return err
		}
	}
	if worker.hController.HasOffloader(nsKey) {
		if err := worker.hController.DeleteOffldSvc(nsKey); err != nil {
			return err
		}
	}

	if !worker.isVarnishIngSvc(svc) {
		return worker.enqueueIngressForService(svc)
	}

	return nil
}
