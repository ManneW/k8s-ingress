/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package haproxy

// XXX:
// - monitor
// - metrics

import (
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"code.uplex.de/uplex-varnish/k8s-ingress/pkg/interfaces"

	"github.com/sirupsen/logrus"
)

const linuxNameMax = 255

// SecretSpec specifies an Ingress TLS Secret for the purposes of the
// haproxy controller. It suffices to identify the *exact* k8s
// configuration of the Secret, including UID and ResourceVersion.
type SecretSpec struct {
	Namespace       string
	Name            string
	UID             string
	ResourceVersion string
}

func (spec SecretSpec) String() string {
	return spec.Namespace + "/" + spec.Name
}

// Spec specifies the configuration of TLS offload for haproxy. It
// includes the namespace and name of the Varnish admin Service (the
// headless k8s Service specifying ports for remote administration),
// and a list of specs for Ingress TLS Secrets.
type Spec struct {
	Namespace string
	Name      string
	Secrets   []SecretSpec
}

func (spec Spec) String() string {
	return spec.Namespace + "/" + spec.Name
}

func (spec SecretSpec) hashedName() string {
	hash := sha512.New512_256()
	hash.Write([]byte(spec.Namespace))
	hash.Write([]byte(spec.Name))
	bytes := hash.Sum(nil)
	return base64.RawURLEncoding.EncodeToString(bytes)
}

// CertName returns a name for a PEM file to be used in the haproxy
// ssl configuration (concatenated from crt and key), formed from
// the Namespace and Name of the TLS Secret. The name has the .pem
// extension.
//
// Guaranteed to be shorter than NAME_MAX on recent common Linux
// distributions; so it may be an opaque hash string, if the Namespace
// and/or Name are too long.
func (spec SecretSpec) CertName() string {
	name := spec.Namespace + "_" + spec.Name
	if len(name) > linuxNameMax {
		name = spec.hashedName()
	}
	return name + ".pem"
}

// OffldAddr encapsulate the networking information for remote
// administration of a TLS offloader for Ingress, implemented by
// haproxy.
//
// Includes the namespace and name of the Pod in which haproxy runs,
// and the addresses of the dataplane API and http-faccess server.
type OffldAddr struct {
	PodNamespace  string
	PodName       string
	IP            string
	DataplanePort int32
	FaccessPort   int32
}

// OffldrError encapsulates an error in the interaction of the haproxy
// controller with a haproxy container.
type OffldrError struct {
	addr string
	name string
	err  error
}

func (offldrErr OffldrError) Error() string {
	return fmt.Sprintf("name=%s addr=%s: %v", offldrErr.name,
		offldrErr.addr, offldrErr.err)
}

// OffldrErrors encapsulates a group of errors in the interaction with
// a haproxy container. Most of the controllers actions apply to all
// of the replicas in a Pod, and the controller does not stop at the
// first error.  So any errors encountered along the way, are
// collected and returned by this type. This makes it possible for an
// action to succeed for some of the replicas. If an action had no
// error, usually nil is returned for the error value (rather than an
// empty slice).
//
// This type satisfies the error interface.
type OffldrErrors []OffldrError

func (offldrErrs OffldrErrors) Error() string {
	var sb strings.Builder
	sb.WriteRune('[')
	for _, err := range offldrErrs {
		sb.WriteRune('{')
		sb.WriteString(err.Error())
		sb.WriteRune('}')
	}
	sb.WriteRune(']')
	return sb.String()
}

type configStatus struct {
	dplaneState ReloadState
	version     int64
	pemExists   map[SecretSpec]bool
	loaded      bool
}

type haproxyInst struct {
	spec         *Spec
	dplane       *DataplaneClient
	faccess      *FaccessClient
	dplanePasswd *string
	admMtx       *sync.Mutex
	status       configStatus
	name         string
}

type offldrSvc struct {
	instances []*haproxyInst
	spec      *Spec
	secrName  string
}

// Controller (or haproxy controller) remotely administers a haproxy
// container to configure TLS offload for Ingress. For the most part,
// this is done with the dataplane API -- see the documentaion of
// DataplaneClient, and the links shown there.
type Controller struct {
	log      *logrus.Logger
	svcEvt   interfaces.SvcEventGenerator
	svcs     map[string]*offldrSvc
	secrets  map[string]*string
	wg       *sync.WaitGroup
	monIntvl time.Duration
}

// NewOffloaderController returns a controller to remotely administer
// a haproxy container for Ingress TLS offload, logging its work with
// the given logger.
//
// XXX monIntvl is meant to be the interval for a monitor loop,
// analogous to the monitor for Varnish instances; currently not
// implemented.
func NewOffloaderController(
	log *logrus.Logger, monIntvl time.Duration) *Controller {

	// XXX initMetrics()
	return &Controller{
		svcs:     make(map[string]*offldrSvc),
		secrets:  make(map[string]*string),
		log:      log,
		monIntvl: monIntvl,
		wg:       new(sync.WaitGroup),
	}
}

// Start initiates a haproxy controller.
//
// XXX currently little more than a no-op, will start the monitor
func (hc *Controller) Start() {
	fmt.Printf("Offloader controller logging at level: %s\n", hc.log.Level)
	// go hc.monitor(hc.monIntvl)
}

// HasOffloader returns true iff the controller has configured the TLS
// offloader designated by svcKey.
func (hc *Controller) HasOffloader(svcKey string) bool {
	_, exists := hc.svcs[svcKey]
	return exists
}

// XXX eliminate repetition in updateLoadStatus() & updateInstance()
func (hc *Controller) updateLoadStatus(inst *haproxyInst) error {
	hc.log.Debugf("Offloader instance %s, checking load status, spec: %+v",
		inst.name, inst.spec)
	inst.admMtx.Lock()
	defer inst.admMtx.Unlock()
	hc.wg.Add(1)
	defer hc.wg.Done()

	for _, s := range inst.spec.Secrets {
		pemExists, ok := inst.status.pemExists[s]
		if ok && pemExists {
			continue
		}
		if !ok {
			inst.status.pemExists[s] = false
		}
		hc.log.Debugf("Offloader instance %s, checking for PEM file %s",
			inst.name, s.CertName())
		pemExists, err := inst.faccess.PEMExists(s)
		if err != nil {
			// XXX decode?
			return err
		}
		inst.status.pemExists[s] = pemExists
		if !pemExists {
			// XXX SyncIncomplete
			return fmt.Errorf("Offloader instance %s: certificate "+
				"PEM %s not found", inst.name, s.CertName())
		}
		hc.log.Infof("Offloader instance %s: PEM file %s exists",
			inst.name, s.CertName())
		// XXX initiate load
	}

	if inst.status.dplaneState.ID == "" {
		hc.log.Infof("Offloader instance %s: no known TLS config "+
			"reload state", inst.name)
		return nil
	}

	hc.log.Debugf("Offloader instance %s: checking reload state: %s",
		inst.name, inst.status.dplaneState.ID)
	reloaded, state, err := inst.dplane.Reloaded(inst.status.dplaneState.ID)
	if err != nil {
		return err
	}
	inst.status.dplaneState = state
	if !reloaded {
		// XXX SyncIncomplete
		return fmt.Errorf("Offloader instance %s: TLS config %s not "+
			"loaded, status: %s", inst.name, inst.spec,
			state.Status)
	}
	hc.log.Infof("Offloader instance %s: TLS config %s successfully "+
		"loaded at %s", inst.name, inst.spec,
		state.Timestamp.Format(time.RFC3339))
	return nil
}

func (hc *Controller) updateInstance(inst *haproxyInst, spec *Spec) error {
	var err error

	hc.log.Infof("Update offloader instance %s to TLS config: %s",
		inst.name, spec)
	hc.log.Debugf("Offloader instance: %+v", inst)
	if reflect.DeepEqual(spec, inst.spec) {
		hc.log.Infof("Offloader instance %s: TLS config %s was "+
			"accepted for load", inst.name, spec)
		return hc.updateLoadStatus(inst)
	}
	hc.log.Debugf("Offloader instance %s, old spec: %+v, new spec: %+v",
		inst.name, inst.spec, spec)

	inst.admMtx.Lock()
	defer inst.admMtx.Unlock()
	hc.wg.Add(1)
	defer hc.wg.Done()

	if len(spec.Secrets) == 0 {
		hc.log.Warnf("Offloader instance %s: no certificates specified",
			inst.name)
		return nil
	}
	for _, s := range spec.Secrets {
		hc.log.Debugf("Offloader instance %s, checking for PEM file %s",
			inst.name, s.CertName())
		pemExists, err := inst.faccess.PEMExists(s)
		if err != nil {
			// XXX decode?
			return err
		}
		inst.status.pemExists[s] = pemExists
		if !pemExists {
			// XXX SyncIncomplete
			return fmt.Errorf("Offloader instance %s: certificate "+
				"PEM %s not found", inst.name, s.CertName())
		}
		hc.log.Infof("Offloader instance %s: PEM file %s exists",
			inst.name, s.CertName())
	}

	version := inst.status.version
	if version == 0 {
		version = 1
	}
	hc.log.Debugf("Offloader instance %s: %+v", inst.name, inst)
	hc.log.Debugf("Offloader instance %s: starting tx for version %d",
		inst.name, version)
	tx, err := inst.dplane.StartTx(version)
	if err != nil {
		return err
	}
	hc.log.Debugf("Offloader instance %s: started transaction: %+v",
		inst.name, tx)

	if tx.Version <= 1 {
		hc.log.Debugf("Offloader instance %s: adding TLS config %s",
			inst.name, spec)
		err = inst.dplane.AddTLS(tx, *spec)
	} else {
		hc.log.Debugf("Offloader instance %s: updating TLS config %s",
			inst.name, spec)
		err = inst.dplane.UpdateTLS(tx, *spec)
	}
	if err != nil {
		return err
	}

	hc.log.Debugf("Offloader instance %s: finishing tx for version %d: %+v",
		inst.name, tx.Version, tx)
	state, err := inst.dplane.FinishTx(tx)
	if err != nil {
		return err
	}
	hc.log.Infof("Offloader instance %s: TLS config %s accepted for load",
		inst.name, spec)
	hc.log.Debugf("Offloader instance %s reload state: %+v", inst.name,
		state)
	inst.status.version = tx.Version
	inst.status.dplaneState = state
	inst.spec = &Spec{
		Namespace: spec.Namespace,
		Name:      spec.Name,
		Secrets:   make([]SecretSpec, len(spec.Secrets)),
	}
	for i, s := range spec.Secrets {
		inst.spec.Secrets[i] = s
	}

	// XXX where does this go?
	// defer hc.dataplane.DeleteTx(tx)

	switch state.Status {
	case Succeeded:
		inst.status.dplaneState = state
		hc.log.Infof("Offloader instance %s: TLS config %s sucessfully"+
			"loaded at %s", inst.name, spec,
			state.Timestamp.Format(time.RFC3339))
		inst.status.loaded = true
		return nil
	case Failed, Unknown:
		return fmt.Errorf("Offloader instance %s: TLS config %s load "+
			"failed or status unknown: %v", inst.name, spec, state)
	case InProgress:
		inst.status.dplaneState = state
		hc.log.Debugf("Offloader instance %s: checking reload state: "+
			"%s", inst.name, inst.status.dplaneState.ID)
		reloaded, state, err := inst.dplane.Reloaded(
			inst.status.dplaneState.ID)
		if err != nil {
			return err
		}
		inst.status.dplaneState = state
		if reloaded {
			hc.log.Infof("Offloader instance %s: TLS config %s "+
				"successfully loaded at %s", inst.name, spec,
				state.Timestamp.Format(time.RFC3339))
			inst.status.loaded = true
			return nil
		}
		// XXX SyncIncomplete
		return fmt.Errorf("Offloader instance %s: TLS config %s not "+
			"loaded, status: %s", inst.name, spec, state.Status)
	default:
		panic("Illegal reload status")
	}
}

func (inst *haproxyInst) mkError(err error) OffldrError {
	return OffldrError{
		addr: inst.dplane.getHost(),
		name: inst.name,
		err:  err,
	}
}

func (hc *Controller) updateOffldSvc(svcKey string) error {
	var errs OffldrErrors
	svc, exists := hc.svcs[svcKey]
	if !exists || svc == nil {
		return fmt.Errorf("No known offloader service for %s", svcKey)
	}
	if svc.secrName == "" {
		return fmt.Errorf("No known admin secret for offloader service"+
			" %s", svcKey)
	}
	if svc.spec == nil {
		hc.log.Infof("Offloader service %s: no current spec", svcKey)
		return nil
	}

	hc.log.Info("Updating offloader instances for ", svcKey)
	for _, inst := range svc.instances {
		if inst == nil {
			hc.log.Errorf("Instance object is nil")
			continue
		}
		// XXX metrics
		hc.log.Debugf("offloader svc %s: get current status", inst.name)
		if err := hc.getOffldStatus(inst); err != nil {
			offldrErr := inst.mkError(err)
			errs = append(errs, offldrErr)
			continue
		}
		if err := hc.updateInstance(inst, svc.spec); err != nil {
			offldrErr := inst.mkError(err)
			errs = append(errs, offldrErr)
		}
	}
	if len(errs) != 0 {
		return errs
	}
	return nil
}

func (hc *Controller) removeOffldrInstances(
	insts []*haproxyInst) (errs OffldrErrors) {

	for _, inst := range insts {
		version := inst.status.version + 1
		hc.log.Debugf("Offloader instance %s: starting tx for version "+
			"%d", inst.name, version)
		tx, err := inst.dplane.StartTx(version)
		if err != nil {
			errs = append(errs, inst.mkError(err))
			continue
		}
		hc.log.Debugf("Offloader instance %s: started transaction: %+v",
			inst.name, tx)

		hc.log.Debugf("Offloader instance %s: deleting TLS config",
			inst.name)
		err = inst.dplane.DeleteTLS(tx)
		if err != nil {
			errs = append(errs, inst.mkError(err))
			continue
		}

		hc.log.Debugf("Offloader instance %s: finishing tx for "+
			"version %d: %+v", inst.name, tx.Version, tx)
		state, err := inst.dplane.FinishTx(tx)
		if err != nil {
			errs = append(errs, inst.mkError(err))
			continue
		}
		defer inst.dplane.DeleteTx(tx)
		hc.log.Infof("Offloader instance %s: transaction accepted for "+
			"delete", inst.name)
		inst.status.version = tx.Version
		inst.status.dplaneState = state

		switch state.Status {
		case Succeeded:
			hc.log.Infof("Offloader instance %s: TLS config "+
				"sucessfully deleted at %s", inst.name,
				state.Timestamp.Format(time.RFC3339))
			continue
		case Failed, Unknown:
			err = fmt.Errorf("Offloader instance %s: TLS config "+
				"delete failed or status unknown: %v",
				inst.name, state)
			errs = append(errs, inst.mkError(err))
			continue
		case InProgress:
			hc.log.Debugf("Offloader instance %s: checking reload "+
				"state: %s", inst.name,
				inst.status.dplaneState.ID)
			reloaded, state, err := inst.dplane.Reloaded(
				inst.status.dplaneState.ID)
			if err != nil {
				errs = append(errs, inst.mkError(err))
				continue
			}
			inst.status.dplaneState = state
			if reloaded {
				hc.log.Infof("Offloader instance %s: TLS "+
					"config successfully deleted at %s",
					inst.name,
					state.Timestamp.Format(time.RFC3339))
				// instsGauge.Dec()
				continue
			}
			// XXX SyncIncomplete
			err = fmt.Errorf("Offloader instance %s: TLS config "+
				"not deleted, status: %s", inst.name,
				state.Status)
			errs = append(errs, inst.mkError(err))
			continue
		default:
			panic("Illegal reload status")
		}
	}
	if len(errs) == 0 {
		return nil
	}
	return errs
}

func offldAddr2haproxyInst(addr OffldAddr, dplanePasswd *string) *haproxyInst {
	var passwd string
	if dplanePasswd != nil {
		passwd = *dplanePasswd
	}
	dplaneAddr := addr.IP + ":" + strconv.Itoa(int(addr.DataplanePort))
	dplaneClient := NewDataplaneClient(dplaneAddr, passwd)
	faccessAddr := addr.IP + ":" + strconv.Itoa(int(addr.FaccessPort))
	faccessClient := NewFaccessClient(faccessAddr)
	inst := &haproxyInst{
		dplane:       dplaneClient,
		faccess:      faccessClient,
		name:         addr.PodNamespace + "/" + addr.PodName,
		dplanePasswd: dplanePasswd,
		admMtx:       &sync.Mutex{},
	}
	inst.status.pemExists = make(map[SecretSpec]bool)
	return inst
}

// Used as a map key in updateOffldrAddrs().
type offldaddrs struct {
	dplaneAddr, faccessAddr string
}

func (hc *Controller) updateOffldrAddrs(key string, addrs []OffldAddr,
	passwdPtr *string) error {

	var newInsts, remInsts, keepInsts []*haproxyInst

	svc, exists := hc.svcs[key]
	if !exists {
		panic("No known offloader service " + key)
	}

	updateAddrs := make(map[offldaddrs]OffldAddr)
	prevAddrs := make(map[offldaddrs]*haproxyInst)
	for _, addr := range addrs {
		key := offldaddrs{
			dplaneAddr: addr.IP + ":" +
				strconv.Itoa(int(addr.DataplanePort)),
			faccessAddr: addr.IP + ":" +
				strconv.Itoa(int(addr.FaccessPort)),
		}
		updateAddrs[key] = addr
	}
	for _, inst := range svc.instances {
		key := offldaddrs{
			dplaneAddr:  inst.dplane.getHost(),
			faccessAddr: inst.faccess.getHost(),
		}
		prevAddrs[key] = inst
	}
	for addr := range updateAddrs {
		inst, exists := prevAddrs[addr]
		if exists {
			keepInsts = append(keepInsts, inst)
			continue
		}
		newInst := offldAddr2haproxyInst(updateAddrs[addr], passwdPtr)
		newInsts = append(newInsts, newInst)
	}
	for addr, inst := range prevAddrs {
		_, exists := updateAddrs[addr]
		if !exists {
			remInsts = append(remInsts, inst)
		}
	}
	hc.log.Debugf("Varnish offloader svc %s: keeping instances=%+v, "+
		"new instances=%+v, removing instances=%+v", key, keepInsts,
		newInsts, remInsts)
	svc.instances = append(keepInsts, newInsts...)

	errs := hc.removeOffldrInstances(remInsts)

	hc.log.Tracef("Offloader svc %s config: %+v", key, *svc)
	updateErrs := hc.updateOffldSvc(key)
	if updateErrs != nil {
		offldrErrs, ok := updateErrs.(OffldrErrors)
		if ok {
			errs = append(errs, offldrErrs...)
		} else {
			return updateErrs
		}
	}
	if len(errs) == 0 {
		return nil
	}
	return errs
}

func (hc *Controller) getOffldStatus(inst *haproxyInst) error {
	hc.log.Debugf("Offloader instance %s, checking offloader config",
		inst.name)
	inst.admMtx.Lock()
	defer inst.admMtx.Unlock()
	hc.wg.Add(1)
	defer hc.wg.Done()

	loaded, version, err := inst.dplane.OffldrStatus()
	if err != nil {
		return err
	}
	inst.status.version = int64(version)
	inst.status.loaded = loaded
	if loaded {
		hc.log.Infof("Offloader instance %s: offloader configured, "+
			"version=%d", inst.name, version)
	} else {
		hc.log.Infof("Offloader instance %s: offloader not configured,"+
			" current version=%d", inst.name, version)
	}
	return nil
}

// AddOrUpdateOffloader sets the configuration for the offloader
// designated by key, using the given addresses for remote admin, and
// the Secret designated by secrName as the password for Basic Auth in
// requests to the dataplane API.
func (hc *Controller) AddOrUpdateOffloader(key string, addrs []OffldAddr,
	secrName string) error {

	var passwdPtr *string
	svc, exists := hc.svcs[key]
	if !exists {
		var instances []*haproxyInst
		svc = &offldrSvc{}
		for _, addr := range addrs {
			instance := offldAddr2haproxyInst(addr, nil)
			hc.log.Debugf("offloader svc %s: creating instance %+v",
				key, *instance)
			instances = append(instances, instance)
			// instsGauge.Inc()
		}
		svc.instances = instances
		hc.svcs[key] = svc
		// svcsGauge.Inc()
		hc.log.Debugf("offloader svc %s: created config", key)
	}
	hc.log.Debugf("offloader svc %s config: %+v", key, svc)

	svc.secrName = secrName
	if _, exists := hc.secrets[secrName]; exists {
		passwdPtr = hc.secrets[secrName]
	}
	for _, inst := range svc.instances {
		inst.dplanePasswd = passwdPtr
	}
	hc.log.Debugf("offloader svc %s: updated with secret %s", key, secrName)

	hc.log.Debugf("Update offloader svc %s: addrs=%+v secret=%s", key,
		addrs, secrName)
	return hc.updateOffldrAddrs(key, addrs, passwdPtr)
}

// DeleteOffldSvc removes the TLS offloader service designated by
// svcKey -- the haproxy configuration is deleted, and the
// specification is removed from the controller's configuration.
func (hc *Controller) DeleteOffldSvc(svcKey string) error {
	svc, exists := hc.svcs[svcKey]
	if !exists {
		return nil
	}
	err := hc.removeOffldrInstances(svc.instances)
	if err != nil {
		delete(hc.svcs, svcKey)
		// svcsGauge.Dec()
	}
	return err
}

// Update the TLS offloader designated by svcKey to the configuration
// given by spec.
func (hc *Controller) Update(svcKey string, spec Spec) error {
	svc, exists := hc.svcs[svcKey]
	if !exists {
		svc = &offldrSvc{instances: make([]*haproxyInst, 0)}
		hc.svcs[svcKey] = svc
		// svcsGauge.Inc()
		hc.log.Infof("Added offloader service definition %s", svcKey)
	}
	svc.spec = &spec
	if len(svc.instances) == 0 {
		return fmt.Errorf("Currently no known offloader endpoints for "+
			"Service %s", svcKey)
	}
	return hc.updateOffldSvc(svcKey)
}

// SetDataplaneSecret stores the secret to be used as the Basic Auth
// password used in requests to a dataplane API, under the name given
// in key (from the namespace/name of a k8s Secret).
func (hc *Controller) SetDataplaneSecret(key string, secret []byte) {
	_, exists := hc.secrets[key]
	if !exists {
		s := string(secret)
		hc.secrets[key] = &s
		// secretsGauge.Inc()
		return
	}
	*hc.secrets[key] = string(secret)
}

// SetOffldSecret specifies secretKey as the name of the Secret to be
// used to authorize use of the dataplane API for the TLS offloader
// designated by SetOffldSecret. SetDataplaneSecret(), in turns, sets
// the secret contents for secretKey.
func (hc *Controller) SetOffldSecret(svcKey, secretKey string) error {
	svc, ok := hc.svcs[svcKey]
	if !ok {
		hc.log.Warnf("Cannot set secret %s for offloader %s: "+
			"offloader not found", secretKey, svcKey)
		return nil
	}
	svc.secrName = secretKey
	if secret, ok := hc.secrets[secretKey]; ok {
		for _, inst := range svc.instances {
			inst.dplane.password = *secret
		}
	}
	return nil
}

// DeleteDataplaneSecret removes the Secret designated by name.
func (hc *Controller) DeleteDataplaneSecret(name string) {
	_, exists := hc.secrets[name]
	if !exists {
		return
	}
	delete(hc.secrets, name)
	// secretsGauge.Dec()
}

// Quit stops the offloader controller.
func (hc *Controller) Quit() {
	hc.log.Info("Wait for admin interactions with offloader instances to " +
		"finish")
	hc.wg.Wait()
}
