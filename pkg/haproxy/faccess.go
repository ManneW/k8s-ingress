/*
 * Copyright (c) 2020 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved
 *
 * Author: Geoffrey Simmons <geoffrey.simmons@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

package haproxy

import (
	"net/http"
	"net/url"
)

// FaccessError encapsulates an error response from an faccess server.
// Satisfies the error interface.
type FaccessError struct {
	Status int
	PEM    string
}

func (err FaccessError) Error() string {
	return err.PEM + ": " + http.StatusText(err.Status)
}

// FaccessClient sends requests to an http-faccess server to determine
// if a file exists and is readable. This is used to find out when a
// PEM file exists (as projected into a SecretVolume), so that haproxy
// can be reloaded with the TLS certificate in its configuration.
//
//	https://code.uplex.de/testing/http-faccess
type FaccessClient struct {
	baseURL *url.URL
	client  *http.Client
}

// NewFaccessClient returns a client for the http-faccess server
// listening at host.
//
// host may have the form "addr" or "addr:port", where addr may be a
// host name or IP address.
func NewFaccessClient(host string) *FaccessClient {
	return &FaccessClient{
		baseURL: &url.URL{
			Scheme: "http",
			Host:   host,
		},
		client: http.DefaultClient,
	}
}

func (client *FaccessClient) getHost() string {
	return client.baseURL.Host
}

// PEMExists returns true iff the http-faccess server reports that the
// PEM file corresponding to spec exists and is readable.
//
// A non-nil error return may wrap FaccessError.
func (client *FaccessClient) PEMExists(spec SecretSpec) (bool, error) {
	relPath := &url.URL{Path: spec.CertName()}
	url := client.baseURL.ResolveReference(relPath)
	req, err := http.NewRequest(http.MethodHead, url.String(), nil)
	if err != nil {
		return false, err
	}

	resp, err := client.client.Do(req)
	if err != nil {
		return false, err
	}
	defer drainAndClose(resp.Body)

	switch resp.StatusCode {
	case http.StatusNoContent:
		return true, nil
	case http.StatusNotFound:
		return false, nil
	default:
		return false, &FaccessError{
			Status: resp.StatusCode,
			PEM:    spec.CertName(),
		}
	}
}
