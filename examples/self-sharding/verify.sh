#! /bin/bash -ex

# Nothing special about self-sharding is verified here, just run the
# same tests as for the cafe example ("hello").

function killforward {
    kill $KUBEPID
}

LOCALPORT=${LOCALPORT:-8888}

kubectl wait --timeout=2m pod -l app=varnish-ingress --for=condition=Ready

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:80 >/dev/null &
KUBEPID=$!
trap killforward EXIT

# XXX hackish
# If we run the test too "soon" after the Varnish Services become ready,
# the client may received a 503 response. It doesn't appear to happen if
# we wait a few seconds longer.
# For now, wait longer to run the verification. Should investigate why
# Varnish is not properly configured immediately after self-sharding is
# deployed.
sleep 10
#sleep 1
varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
