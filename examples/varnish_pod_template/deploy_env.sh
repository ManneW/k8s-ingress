#! /bin/bash -ex

kubectl apply -f ../hello/cafe.yaml

kubectl apply -f ../hello/cafe-ingress.yaml

kubectl delete -f ../../deploy/admin-svc.yaml

kubectl delete deploy varnish

echo Waiting until example varnish-ingress Pods are deleted
kubectl wait --timeout=2m pod -l app=varnish-ingress --for=delete

kubectl apply -f env.yaml
