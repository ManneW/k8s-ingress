#! /bin/bash -ex

kubectl delete -f beverage-ingress.yaml

kubectl delete -f beverage.yaml

kubectl delete -f bar-tls-secret.yaml

kubectl delete -f cafe-tls-secret.yaml

echo "Waiting until varnish-ingress Pods are not ready"
JSONPATH='{range .items[*]}{@.metadata.name}:{range @.status.conditions[*]}{@.type}={@.status};{end}{end}'

N=0
until [ $N -ge 120 ]
do
    if kubectl get pods -l app=varnish-ingress -o jsonpath="${JSONPATH}" | grep -q '\bReady=True\b'; then
        sleep 10
        N=$(( N + 10 ))
        continue
    fi
    exit 0
done
echo "Giving up"
exit 1
