# looks like -*- vcl -*-

varnishtest "cafe example with TLS offload"

feature ignore_unknown_macro

feature cmd {haproxy --version 2>&1 | grep -q 'HA-Proxy version'}

# Use haproxy to forward the non-TLS request from the VTC client below to
# the Ingress TLS port.
haproxy h1 -conf {
    defaults
	mode   http
	timeout connect         5s
	timeout server          30s
	timeout client          30s

    backend ingress
	server ingress ${localhost}:${localport} ssl verify none

    frontend http1
	use_backend ingress
	bind "fd@${fe1}"
	acl backend_tls ssl_bc
	http-response add-header X-Backend-TLS "true" if backend_tls
} -start

client c1 -connect ${h1_fe1_sock} {
	txreq -url /coffee/foo/bar -hdr "Host: cafe.example.com"
	rxresp
	expect resp.status == 200
	expect resp.body ~ "(?m)^URI: /coffee/foo/bar$"
	expect resp.body ~ "(?m)^Server name: coffee-[a-z0-9]+-[a-z0-9]+$"
	expect resp.http.X-Backend-TLS == "true"

	txreq -url /tea/baz/quux -hdr "Host: cafe.example.com"
	rxresp
	expect resp.status == 200
	expect resp.body ~ "(?m)^URI: /tea/baz/quux$"
	expect resp.body ~ "(?m)^Server name: tea-[a-z0-9]+-[a-z0-9]+$"
	expect resp.http.X-Backend-TLS == "true"

	txreq -url /coffee/foo/bar
	rxresp
	expect resp.status == 404
	expect resp.http.X-Backend-TLS == "true"

        # Connection:close reduces (but does not entirely eliminate) error
        # messages from kubectl port-forward: EPIPE and ECONNRESET
	txreq -url /milk -hdr "Host: cafe.example.com" -hdr "Connection: close"
	rxresp
	expect resp.status == 404
	expect resp.http.X-Backend-TLS == "true"
} -run
