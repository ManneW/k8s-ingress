#! /bin/bash -x

function killforward {
    kill $KUBEPID
}

LOCALPORT=${LOCALPORT:-4443}

# Long timeout to wait for the Secret to appear as a certificate on
# the Pods.
kubectl wait --timeout=5m pod -l app=varnish-ingress --for=condition=Ready
ret=$?
if [ $ret -ne 0 ]; then
    exit $ret
fi

kubectl port-forward svc/varnish-ingress ${LOCALPORT}:443 >/dev/null &
ret=$?
if [ $ret -ne 0 ]; then
    exit $ret
fi
KUBEPID=$!
trap killforward EXIT

sleep 1
# The test may be skipped (exit status 77) if haproxy is not installed.
varnishtest ${TESTOPTS} -Dlocalport=${LOCALPORT} cafe.vtc
ret=$?
if [ $ret -eq 77 ]; then
    exit 0
elif [ $ret -ne 0 ]; then
    exit $ret
fi

exit 0

