#!/bin/bash

set -e
set -u

/bin/sed "s/%%SECRET_DATAPLANEAPI%%/${SECRET_DATAPLANEAPI}/g" /etc/haproxy/haproxy.cfg > /run/haproxy/haproxy.cfg

exec /usr/sbin/haproxy -f /run/haproxy/haproxy.cfg "$@"
