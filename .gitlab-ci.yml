stages:
  - test
  - build
  - e2e
  - release

push_to_docker:
  image: docker:19.03.8
  stage: release
  only:
    - tags
  services:
    - docker:19.03.8-dind
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE/varnish-ingress/haproxy:$CI_COMMIT_REF_NAME
    - docker pull $CI_REGISTRY_IMAGE/varnish-ingress/varnish:$CI_COMMIT_REF_NAME
    - docker pull $CI_REGISTRY_IMAGE/varnish-ingress/controller:$CI_COMMIT_REF_NAME
    - docker tag $CI_REGISTRY_IMAGE/varnish-ingress/haproxy:$CI_COMMIT_REF_NAME uplex/viking-haproxy:$CI_COMMIT_TAG
    - docker tag $CI_REGISTRY_IMAGE/varnish-ingress/varnish:$CI_COMMIT_REF_NAME uplex/viking-varnish:$CI_COMMIT_TAG
    - docker tag $CI_REGISTRY_IMAGE/varnish-ingress/controller:$CI_COMMIT_REF_NAME uplex/viking-controller:$CI_COMMIT_TAG
    - docker login -u "$DOCKERHUB_USER" -p "$DOCKERHUB_CREDS"
    - docker push uplex/viking-haproxy:$CI_COMMIT_TAG
    - docker push uplex/viking-varnish:$CI_COMMIT_TAG
    - docker push uplex/viking-controller:$CI_COMMIT_TAG

test:
  image: golang:1.11.6
  stage: test
  rules:
    - changes:
      - cmd/**.go
      - pkg/**.go
      - go.mod
      - go.sum
  before_script:
    - GO111MODULE=off go get -d github.com/slimhazard/gogitversion
    - pushd $GOPATH/src/github.com/slimhazard/gogitversion*
    - make install
    - popd
    - go get -u golang.org/x/lint/golint
    - go mod download
  script:
    - make check

# build new haproxy image if dockerfile changes
build:haproxy:
  extends: .build-haproxy
  rules:
    - changes:
      - container/Dockerfile.haproxy

# haproxy image can be build when user triggers it
build:haproxy:manual:
  extends: .build-haproxy
  when: manual

# build new klarlack image if dockerfile changes
build:klarlack:
  extends: .build-klarlack
  rules:
    - changes:
      - container/Dockerfile.klarlack

# klarlack image can be build when user triggers it
build:klarlack:manual:
  extends: .build-klarlack
  when: manual

# build new varnish image if dockerfile changes
build:varnish:
  extends: .build-varnish
  rules:
    - changes:
      - container/Dockerfile.varnish

# varnish image can be build when user triggers it
build:varnish:manual:
  extends: .build-varnish
  when: manual

build:controller:
  extends: .build-image
  stage: build
  variables:
    IMAGE: controller
  script:
    - cd container
    - make controller

example:hello:
  extends: .integration-tests
  script:
    - cd ../examples/hello
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:namespace:
  extends: .integration-tests
  script:
    - cd ../examples/namespace
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:architectures:cluster-and-ns-wide:
  extends: .integration-tests
  script:
    - cd ../examples/architectures/cluster-and-ns-wide
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:architectures:clusterwide:
  extends: .integration-tests
  script:
    - cd ../examples/architectures/clusterwide
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:architectures:multi-controller:
  extends: .integration-tests
  script:
    - cd ../examples/architectures/multi-controller
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:architectures:multi-varnish-ns:
  extends: .integration-tests
  script:
    - cd ../examples/architectures/multi-varnish-ns
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:acl:
  extends: .integration-tests
  script:
    - cd ../examples/acl
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:backend-config:
  extends: .integration-tests
  script:
    - cd ../examples/backend-config
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:custom-vcl:
  extends: .integration-tests
  script:
    - cd ../examples/custom-vcl
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:rewrite:
  extends: .integration-tests
  script:
    - cd ../examples/rewrite
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:self-sharding:
  extends: .integration-tests
  script:
    - cd ../examples/self-sharding
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:authentication:basic:
  extends: .integration-tests
  script:
    - cd ../examples/authentication
    - ./deploy_basic_auth.sh
    - ./verify_basic_auth.sh
    - ./undeploy_basic_auth.sh

example:authentication:acl-or-auth:
  extends: .integration-tests
  script:
    - cd ../examples/authentication
    - ./deploy_acl_or_auth.sh
    - ./verify_acl_or_auth.sh
    - ./undeploy_acl_or_auth.sh

example:authentication:proxy:
  extends: .integration-tests
  script:
    - cd ../examples/authentication
    - ./deploy_proxy_auth.sh
    - ./verify_proxy_auth.sh
    - ./undeploy_proxy_auth.sh

example:req-disposition:
  extends: .integration-tests
  script:
    - cd ../examples/req-disposition
    - ./deploy_alt-builtin.sh
    - ./verify_alt-builtin.sh
    - ./undeploy_alt-builtin.sh

    - ./deploy_builtin.sh
    - ./verify_builtin.sh
    - ./undeploy_builtin.sh

    - ./deploy_cacheability.sh
    - ./verify_cacheability.sh
    - ./undeploy_cacheability.sh

    - ./deploy_pass-on-session-cookie.sh
    - ./verify_pass-on-session-cookie.sh
    - ./undeploy_pass-on-session-cookie.sh

    - ./deploy_url-whitelist.sh
    - ./verify_url-whitelist.sh
    - ./undeploy_url-whitelist.sh

    - ./deploy_purge.sh
    - ./verify_purge.sh
    - ./undeploy_purge.sh

example:varnish-pod-template:
  extends: .integration-tests
  script:
    - cd ../examples/varnish_pod_template
    - ./deploy_cli-args.sh
    - ./verify_cli-args.sh

    - ./deploy_env.sh
    - ./verify_env.sh

    - ./deploy_proxy.sh
    - ./verify_proxy.sh
    - ./undeploy.sh

example:tls:hello:
  extends: .integration-tests
  script:
    - cd ../examples/tls/hello
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

example:tls:sni:
  extends: .integration-tests
  script:
    - cd ../examples/tls/sni
    - ./deploy.sh
    - ./verify.sh
    - ./undeploy.sh

.integration-tests:
  needs: ["build:controller"]
  stage: e2e
  image: docker:19.03.8
  retry: 2
  services:
    - docker:19.03.8-dind
  variables:
    KUBECTL: v1.17.0
    KIND: v0.7.0
  before_script:
    - apk add -U wget
    - apk add -U curl
    - apk add -U varnish
    - apk add -U make
    - apk add -U bash
    - apk add -U haproxy
    - wget -O /usr/local/bin/kind https://github.com/kubernetes-sigs/kind/releases/download/${KIND}/kind-linux-amd64
    - chmod +x /usr/local/bin/kind
    - wget -O /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/${KUBECTL}/bin/linux/amd64/kubectl
    - chmod +x /usr/local/bin/kubectl
    - kind create cluster --config=.kind-gitlab.yaml
    - sed -i -E -e 's/localhost|0\.0\.0\.0/docker/g' "$HOME/.kube/config"
    - kubectl get nodes -o wide
    - kubectl get pods --all-namespaces -o wide
    - kubectl get services --all-namespaces -o wide
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE/varnish-ingress/controller:$CI_COMMIT_SHA
    # using haproxy and varnish image with latest tag due to performance. if we are changing this image a lot, we need to improve this as can lead to pulling the incorrect image
    - docker pull $CI_REGISTRY_IMAGE/varnish-ingress/klarlack:$CI_COMMIT_REF_NAME
    - docker pull $CI_REGISTRY_IMAGE/varnish-ingress/haproxy:$CI_COMMIT_REF_NAME
    - docker tag $CI_REGISTRY_IMAGE/varnish-ingress/controller:$CI_COMMIT_SHA varnish-ingress/controller
    - docker tag $CI_REGISTRY_IMAGE/varnish-ingress/haproxy:$CI_COMMIT_REF_NAME varnish-ingress/haproxy
    - docker tag $CI_REGISTRY_IMAGE/varnish-ingress/klarlack:$CI_COMMIT_REF_NAME varnish-ingress/varnish
    - kind load docker-image varnish-ingress/varnish
    - kind load docker-image varnish-ingress/haproxy
    - kind load docker-image varnish-ingress/controller
    - cd deploy
    - ./init.sh
    - ./deploy.sh
    - ./verify.sh

# basic step for a pipeline to build the varnish image
.build-varnish:
  extends: .build-image
  stage: build
  variables:
    IMAGE: varnish
  script:
    - cd container
    - make varnish

# basic step for a pipeline to build the klarlack image
.build-klarlack:
  extends: .build-image
  stage: build
  variables:
    IMAGE: klarlack
  script:
    - cd container
    - make klarlack

# basic step for a pipeline to build the haproxy image
.build-haproxy:
  extends: .build-image
  stage: build
  variables:
    IMAGE: haproxy
  script:
    - cd container
    - make haproxy

.build-image:
  stage: build
  image: docker:19.03.8
  services:
    - docker:19.03.8-dind
  variables:
    IMAGE: BASIC
  before_script:
    - apk add -U make
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker pull $CI_REGISTRY_IMAGE/varnish-ingress/$IMAGE:$CI_COMMIT_REF_NAME || true
  after_script:
    - docker tag varnish-ingress/$IMAGE $CI_REGISTRY_IMAGE/varnish-ingress/$IMAGE:$CI_COMMIT_SHA
    - docker tag varnish-ingress/$IMAGE $CI_REGISTRY_IMAGE/varnish-ingress/$IMAGE:$CI_COMMIT_REF_NAME
    - docker push $CI_REGISTRY_IMAGE/varnish-ingress/$IMAGE:$CI_COMMIT_SHA
    - docker push $CI_REGISTRY_IMAGE/varnish-ingress/$IMAGE:$CI_COMMIT_REF_NAME
